﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static InstantPayServiceLib.SMBPModel;

namespace InstantPayServiceLib
{
    public static class SMBPDataBase
    {
        #region [Connection Strings]
        public static SqlConnection MyAmdDBConnection = new SqlConnection(InstantPayConfig.MyAmdDBConnectionString);

        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }

        public static void MyAmdOpenConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Closed)
            {
                MyAmdDBConnection.Open();
            }
        }
        public static void MyAmdCloseConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Open)
            {
                MyAmdDBConnection.Close();
            }
        }
        #endregion

        public static DataTable GetMobileOprator(string serviceType)
        {
            string query = "select * from T_SMBP_BBPSBillerList where ServiceType='" + serviceType + "'";

            return GetRecordFromTable(query);
        }

        public static DataTable GetBillerLabel(string fetchid)
        {
            string query = "select b.BillUpdation,l.[Index],l.Labels,l.FieldMinLen,l.FieldMaxLen from T_SMBP_BBPSBillerList b left join T_SMBP_BBPSBillerLabelData l on b.FetchId=l.FetchId where l.FetchId='" + fetchid + "'";

            return GetRecordFromTable(query);
        }




        public static int InsertBBPSPayment(string agentId, string clientRefId, string number, string spKey, string circleID, string amount, List<string> optional = null)
        {
            try
            {
                //string query = "insert into T_SMBP_BBPSPayment (AgentId,ClientRefId,Number,SPKey,CircleID,Amount)"
                //    + " values ('" + agentId + "','" + clientRefId + "','" + number + "','" + spKey + "','" + circleID + "','" + amount + "' ) SELECT SCOPE_IDENTITY()";

                int id = InsertBBPSPaymentDetails("", agentId, clientRefId, number, spKey, circleID, amount, "", "", "", "", "insert", false, "");

                if (id > 0)
                {
                    if (optional != null && optional.Count > 0)
                    {
                        foreach (var opt in optional)
                        {
                            string innerquery = "insert into T_SMBP_BBPSPaymentOptional (AgentId,ClientRefId,Optional)"
                            + "values ('" + agentId + "','" + clientRefId + "','" + opt + "')";

                            InsertUpdateDataBase(innerquery);
                        }
                    }
                }

                return id;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 0;
        }

        public static int UpdateBBPSPayment(string lstPayId, string transactionId, string avlBalance, string transId, string status, bool refund = false, string refundid = null)
        {
            return InsertBBPSPaymentDetails(lstPayId, "", "", "", "", "", "", transactionId, avlBalance, transId, status, "update", refund, refundid);

            //string innerquery = "update T_SMBP_BBPSPayment"
            //                   + " set TransactionId='" + transactionId + "',AvlBalance='" + avlBalance + "',OptTransactionId='" + transId + "',[Status]='" + status + "',UpdatedDate=getdate()"
            //                   + " where BBPSPaymentId=" + id;

            //return InsertUpdateDataBase(innerquery);
        }

        #region [Log Section]
        public static bool Information(string postUrl, string requestRemark, string requestJson, string responseJson, string actionType, string agentId, string clientRefId)
        {
            try
            {
                string query = "insert into T_SMBPResponseLog (PostUrl,RequestRemark,RequestJson,ResponseJson,ActionType,AgentId,ClientRefId)"
                    + "values ('" + postUrl + "','" + requestRemark + "','" + requestJson + "','" + responseJson + "','" + actionType + "','" + agentId + "','" + clientRefId + "')";

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool Error(string requestRemark, string postUrl, string requestJson, string responseRemark, string errorRemark, string actionType, string agentId, string clientRefId)
        {
            try
            {
                string query = "insert into T_SMBPErrorLog (RequestRemark,PostUrl,RequestJson,ResponseRemark,ErrorRemark,ActionType,AgentId)"
                    + "values ('" + requestRemark + "','" + postUrl + "','" + requestJson + "','" + responseRemark + "','" + errorRemark + "','" + actionType + "','" + agentId + "','" + clientRefId + "')";

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        #endregion

        #region [Common Section]
        private static int InsertBBPSPaymentDetails(string payid, string agentId, string crefid, string number, string spkey, string circleid, string amt, string trnsid, string bal, string opttransId, string status, string type, bool refund = false, string refundid = null)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_SMBPInsertUpdatePayment", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@BBPSPaymentId", !string.IsNullOrEmpty(payid) ? payid : ""));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", !string.IsNullOrEmpty(agentId) ? agentId : ""));
                sqlCommand.Parameters.Add(new SqlParameter("@ClientRefId", !string.IsNullOrEmpty(crefid) ? crefid : ""));
                sqlCommand.Parameters.Add(new SqlParameter("@Number", !string.IsNullOrEmpty(number) ? number : ""));
                sqlCommand.Parameters.Add(new SqlParameter("@SPKey", !string.IsNullOrEmpty(spkey) ? spkey : ""));
                sqlCommand.Parameters.Add(new SqlParameter("@CircleID", !string.IsNullOrEmpty(circleid) ? circleid : ""));
                sqlCommand.Parameters.Add(new SqlParameter("@Amount", !string.IsNullOrEmpty(amt) ? amt : ""));
                sqlCommand.Parameters.Add(new SqlParameter("@TransactionId", !string.IsNullOrEmpty(trnsid) ? trnsid : ""));
                sqlCommand.Parameters.Add(new SqlParameter("@AvlBalance", !string.IsNullOrEmpty(bal) ? bal : ""));
                sqlCommand.Parameters.Add(new SqlParameter("@OptTransactionId", !string.IsNullOrEmpty(opttransId) ? opttransId : ""));
                sqlCommand.Parameters.Add(new SqlParameter("@Status", !string.IsNullOrEmpty(status) ? status : ""));
                sqlCommand.Parameters.Add(new SqlParameter("@Type", type));
                sqlCommand.Parameters.Add(new SqlParameter("@Refund", refund));
                sqlCommand.Parameters.Add(new SqlParameter("@RefundId", refundid));
                sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;

                MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                string id = sqlCommand.Parameters["@Id"].Value.ToString();
                MyAmdCloseConnection();

                if (isSuccess > 0 && type == "insert")
                {
                    return Convert.ToInt32(id);
                }
                else if (isSuccess > 0)
                {
                    return 1;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 0;
        }

        private static DataTable GetRecordFromTable(string query)
        {
            try
            {
                if (!string.IsNullOrEmpty(query))
                {
                    sqlCommand = new SqlCommand(query, MyAmdDBConnection);

                    sqlDataAdapter = new SqlDataAdapter();
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    dataSet = new DataSet();
                    sqlDataAdapter.Fill(dataSet, "T_Table");
                    dataTable = dataSet.Tables["T_Table"];
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        private static bool InsertUpdateDataBase(string query)
        {
            try
            {
                if (!string.IsNullOrEmpty(query))
                {
                    sqlCommand = new SqlCommand(query, MyAmdDBConnection);

                    MyAmdOpenConnection();
                    int isSuccess = sqlCommand.ExecuteNonQuery();
                    MyAmdCloseConnection();

                    if (isSuccess > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        private static int InsertIntoDataBaseRutId(string query)
        {
            try
            {
                if (!string.IsNullOrEmpty(query))
                {
                    sqlCommand = new SqlCommand(query, MyAmdDBConnection);

                    MyAmdOpenConnection();
                    int isSuccess = sqlCommand.ExecuteNonQuery();
                    MyAmdCloseConnection();

                    return isSuccess;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 0;
        }
        #endregion

        #region [Don't Remove this section]
        //public static bool InsertBBPSBillerList(BBPSBillerList bbpsBillerList, string agentId)
        //{
        //    bool isSuccess = false;
        //    try
        //    {
        //        foreach (var item in bbpsBillerList.BillerData)
        //        {
        //            string fetchId = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 15).ToUpper();

        //            string query = "insert into T_SMBP_BBPSBillerList (ServiceType,Operator,SpKey,IsBillFetch,BillUpdation,BillerId,AgentId,FetchId)"
        //            + "values ('" + item.ServiceType + "','" + item.Operator + "','" + item.SpKey + "','" + item.IsBillFetch + "','" + item.BillUpdation + "','" + item.BillerId + "','" + agentId + "','" + fetchId + "')";

        //            if (InsertUpdateDataBase(query))
        //            {
        //                isSuccess = true;

        //                foreach (var labelData in item.LabelData)
        //                {
        //                    string innerquery = "insert into T_SMBP_BBPSBillerLabelData (BillerId,[Index],Labels,FieldMinLen,FieldMaxLen,AgentId,FetchId)"
        //            + "values ('" + item.BillerId + "','" + labelData.Index + "','" + labelData.Labels + "','" + labelData.FieldMinLen + "','" + labelData.FieldMaxLen + "','" + agentId + "','" + fetchId + "')";

        //                    if (InsertUpdateDataBase(innerquery))
        //                    {
        //                        isSuccess = true;
        //                    }
        //                    else
        //                    {
        //                        isSuccess = false;
        //                        break;
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                isSuccess = false;
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //    }

        //    return isSuccess;
        //}
        #endregion

        public static DataTable GetBillTransactionHistory(string agentId, string fromDate = null, string toDate = null, string clintRefId = null, string transType = null, string status = null)
        {
            string query = "SELECT bp.*, bl.ServiceType, bl.Operator FROM  T_SMBP_BBPSPayment bp inner JOIN T_SMBP_BBPSBillerList bl on bp.SPKey=bl.SpKey ";

            string whereCond = " where bp.AgentId='" + agentId + "' ";
            if (!string.IsNullOrEmpty(fromDate))
            {
                whereCond += " and bp.CreatedDate>=CONVERT(datetime,'" + GetDateFormate(fromDate) + "')";
            }
            if (!string.IsNullOrEmpty(toDate))
            {
                whereCond += " and bp.CreatedDate<=CONVERT(datetime,'" + GetDateFormate(toDate) + " 23:59:59')";
            }
            if (!string.IsNullOrEmpty(clintRefId))
            {
                whereCond += " and bp.ClientRefId='" + clintRefId + "'";
            }
            if (!string.IsNullOrEmpty(transType))
            {
                string type = string.Empty;

                if (transType.ToLower().Trim() == "mobile")
                {
                    whereCond += " and (bl.ServiceType='PREPAID' or bl.ServiceType='MOBILE POSTPAID')";
                }
                else
                {
                    whereCond += " and bl.ServiceType='" + transType + "'";
                }
            }
            if (!string.IsNullOrEmpty(status))
            {
                whereCond += " and bp.Status like '%" + status + "%'";
            }

            whereCond += " order by bp.CreatedDate desc";

            return GetRecordFromTable(query + whereCond);
        }

        private static string GetDateFormate(string date)
        {
            string result = string.Empty;

            if (!string.IsNullOrEmpty(date))
            {
                string[] strDate = date.Split('/');

                result = strDate[2] + "-" + strDate[1] + "-" + strDate[0];
            }

            return result;
        }

        public static bool ProcessToReFund(string transid, string reportid)
        {
            try
            {
                string query = "update T_SMBP_BBPSPayment set Refund=1, RefundId='' where RefundId='" + reportid + "' and BBPSPaymentId=" + transid;

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool UpdateBSSPPaymentByStatus(string billid, string transStatus)
        {
            string innerquery = "update T_SMBP_BBPSPayment set Status='" + transStatus + "', Refund=1, RefundId='' where BBPSPaymentId=" + billid;

            if (InsertUpdateDataBase(innerquery))
            {
                return true;
            }

            return false;
        }
    }
}
