﻿using GoogleMaps.LocationServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using static InstantPayServiceLib.InstantPay_Model;

namespace InstantPayServiceLib
{
    public static class InstantPay_ApiService
    {
        private static string TokenID = "3ea8f8feaafa38d2ade6cfea85861d13";//"   ffecc13de31e6be9fe916654e0bcf61f";
        private static string PostUrl { get; set; }
        private static int OutletId = 1;

        private static string GetPostUrl()
        {
            return "https://www.instantpay.in/ws/dmi/";
        }

        private static string GetInitiatePayoutPostUrl()
        {
            return "https://www.instantpay.in/ws/payouts/";
        }

        private static string GetTrackId()
        {
            return "DMT" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 15).ToUpper();
        }

        #region [API SECTION]
        public static string GetReSendRemitterDetail(string mobile, string agentId)
        {
            string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"mobile\": \"" + mobile + "\",\"outletid\":" + OutletId + "}}";
            PostUrl = GetPostUrl() + "remitter_details";

            return Post("POST", PostUrl, reqJson, "GetRemitterDetail", agentId, GetTrackId());
        }

        public static string GetRemitterDetail(string mobile, string agentId)
        {
            string result = string.Empty;

            try
            {
                string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"mobile\": \"" + mobile + "\",\"outletid\":" + OutletId + "}}";
                PostUrl = GetPostUrl() + "remitter_details";

                result = Post("POST", PostUrl, reqJson, "GetRemitterDetail", agentId, GetTrackId());
                if (!string.IsNullOrEmpty(result))
                {
                    dynamic dyResult = JObject.Parse(result);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        dynamic dyData = dyResult.data;
                        dynamic remitter = dyData.remitter;
                        dynamic beneficiary = dyData.beneficiary;

                        string remitterId = remitter.id;
                        string remittermobile = remitter.mobile;

                        if (!IsRemitterExist(remitterId, remittermobile, agentId))
                        {
                            InsertAlreadyRegRemitterDetails(result, agentId);
                        }

                        //if (!IsRemitterExist(remitterId, remittermobile))
                        //{
                        //    InsertRemitterDetails(result, agentId);
                        //}
                        //else
                        //{
                        //    UpdateInstantPayRemitterRegResponse(result, agentId);
                        //}

                        InsertBeneficiaryDetails(remitterId, agentId, remittermobile, beneficiary);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public static string RemitterRegistration(string mobile, string firstName, string lastName, string pinCode, string localadd, string agentId)
        {
            string result = string.Empty;

            string reqJson = "{\"token\":\"" + TokenID + "\",\"request\": {\"mobile\": \"" + mobile + "\",\"name\":\"" + firstName + "\",\"surname\":\"" + lastName + "\",\"pincode\":\"" + pinCode + "\",\"outletid\":" + OutletId + "}}";
            PostUrl = GetPostUrl() + "remitter";

            //int RemtID = InstantPay_DataBase.InsertRemitterGetId(mobile, firstName, lastName, pinCode, localadd, agentId);
            int RemtID = InstantPay_DataBase.InsertRemitterFirstDetail(mobile, firstName, lastName, pinCode, localadd, agentId);

            if (RemtID > 0)
            {
                result = Post("POST", PostUrl, reqJson, "Remitter_Registration", agentId, GetTrackId());
                //UpdateRemitterRegistration(RemtID, result, agentId);
                UpdateRemitterRegDetail(RemtID, mobile, agentId, result);
            }

            return result;
        }

        public static string RemitterRegistrationValidate(string remitterid, string mobile, string otp, string agentId)
        {
            string result = string.Empty;

            string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"remitterid\": \"" + remitterid + "\",\"mobile\": \"" + mobile + "\",\"otp\": \"" + otp + "\",\"outletid\":" + OutletId + "}}";
            PostUrl = GetPostUrl() + "remitter_validate";

            result = Post("POST", PostUrl, reqJson, "Remitter_Registration_Validate", agentId, GetTrackId());
            if (!string.IsNullOrEmpty(result))
            {
                if (IsRemitterAlreadyExist(remitterid, mobile, agentId))
                {
                    UpdateRemitterRegistrationValidate(result, remitterid, mobile, agentId);
                }
                else
                {
                    string remitterJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"mobile\": \"" + mobile + "\",\"outletid\":" + OutletId + "}}";
                    PostUrl = GetPostUrl() + "remitter_details";

                    result = Post("POST", PostUrl, remitterJson, "GetRemitterDetail", agentId, GetTrackId());
                    if (!string.IsNullOrEmpty(result))
                    {
                        dynamic dyResult = JObject.Parse(result);
                        string statusCode = dyResult.statuscode;
                        string statusMessage = dyResult.status;

                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                        {
                            dynamic dyData = dyResult.data;
                            dynamic remitter = dyData.remitter;
                            dynamic beneficiary = dyData.beneficiary;

                            string remitterId = remitter.id;
                            string remittermobile = remitter.mobile;
                            string firstName = remitter.name;
                            string lastName = "";
                            string pinCode = remitter.pincode;
                            string localadd = "";
                            string kycstatus = remitter.kycstatus;
                            string verifystatus = remitter.is_verified;

                            if (InstantPay_DataBase.InsertRemitterGetId(remittermobile, firstName, lastName, pinCode, localadd, agentId, remitterId, "already registered", kycstatus, verifystatus) > 0)
                            {
                                dynamic remitter_limit = dyData.remitter_limit[0];
                                dynamic remitter_mode = remitter_limit.mode;
                                dynamic remitter_limit_limit = remitter_limit.limit;

                                RemitterRegResponse respo = new RemitterRegResponse();

                                respo.RemitterId = remitter.id;
                                respo.AgentUserId = agentId;
                                respo.Address = remitter.address;
                                respo.City = remitter.city;
                                respo.IsVerified = remitter.is_verified;
                                respo.KYCDoc = remitter.kycdocs;
                                respo.KYCStatus = remitter.kycstatus;
                                respo.Mobile = remitter.mobile;
                                respo.Name = remitter.name;
                                respo.PernTxnLimit = remitter.perm_txn_limit;
                                respo.PinCode = remitter.pincode;
                                respo.State = remitter.state;
                                respo.CreditLimit = remitter_limit_limit.total;
                                respo.ConsumedAmount = remitter_limit_limit.consumed;
                                respo.RemainingLimit = remitter_limit_limit.remaining;
                                respo.IMPSMode = remitter_mode.imps;
                                respo.NEFTMode = remitter_mode.neft;

                                bool isSuccess = InstantPay_DataBase.Insert_T_InstantPayRemitterRegResponse(respo);
                            }
                        }
                    }
                }
            }

            return result;
        }

        private static bool IsRemitterAlreadyExist(string remitterid, string mobile, string agentId)
        {
            return InstantPay_DataBase.IsRemitterAlreadyExist(remitterid, mobile, agentId);
        }

        public static string BeneficiaryRegistration(string remitterid, string name, string mobile, string account, string bankname, string ifsc, string agentId)
        {
            string response = string.Empty;

            string reqJson = "{\"token\": \"" + TokenID + "\", \"request\": {\"remitterid\": \"" + remitterid + "\",\"name\": \"" + name + "\",\"mobile\": \"" + mobile + "\",\"ifsc\": \"" + ifsc + "\",\"account\": \"" + account + "\"}}";
            PostUrl = GetPostUrl() + "beneficiary_register";

            int benId = InsertT_InstantPayBeneficary(remitterid, name, mobile, account, bankname, ifsc, agentId);

            response = Post("POST", PostUrl, reqJson, "Beneficiary_Registration", agentId, GetTrackId());

            if (!string.IsNullOrEmpty(response))
            {
                if (!string.IsNullOrEmpty(response))
                {
                    dynamic dyResult = JObject.Parse(response);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        if (benId > 0)
                        {
                            Update_T_InstantPayBeneficary(benId, response);
                        }
                    }
                    else
                    {
                        InstantPay_DataBase.Delete_T_InstantPayBeneficary(benId);
                    }
                }
            }

            return response;
        }

        public static string BeneficiaryRemove(string beneficiaryid, string remitterid, string agentId)
        {
            string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"beneficiaryid\": \"" + beneficiaryid + "\",\"remitterid\": \"" + remitterid + "\",\"outletid\":" + OutletId + "}}";
            PostUrl = GetPostUrl() + "beneficiary_remove";

            string response = Post("POST", PostUrl, reqJson, "Beneficiary_Remove", agentId, GetTrackId());

            if (!string.IsNullOrEmpty(response))
            {
                dynamic dyResult = JObject.Parse(response);
                string statusCode = dyResult.statuscode;
                string statusMessage = dyResult.status;

                if (statusCode.ToLower() == "err" && statusMessage.ToLower() == "invalid beneficiary id")
                {
                    bool isdeleted = InstantPay_DataBase.DeleteBeneficiaryDetail(beneficiaryid, remitterid, agentId);
                }
            }

            return response;
        }

        public static string BeneficiaryRemoveValidate(string beneficiaryid, string remitterid, string otp, string agentId)
        {
            string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"beneficiaryid\": \"" + beneficiaryid + "\",\"remitterid\": \"" + remitterid + "\",\"otp\": \"" + otp + "\",\"outletid\":" + OutletId + "}}";
            PostUrl = GetPostUrl() + "beneficiary_remove_validate";

            string response = Post("POST", PostUrl, reqJson, "Beneficiary_Remove_Validate", agentId, GetTrackId());

            if (!string.IsNullOrEmpty(response))
            {
                DeleteBeneficiaryDetail(beneficiaryid, remitterid, agentId, response);
            }

            return response;
        }

        public static string FundTransfer(string remittermobile, string beneficiaryid, string amount, string transferMode, string uniqueAgentId, string agentId, string trackid, string RemitterId)
        {
            string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"remittermobile\": \"" + remittermobile + "\",\"beneficiaryid\": \"" + beneficiaryid + "\",\"agentid\": \"" + uniqueAgentId + "\",\"amount\": \"" + amount + "\",\"mode\": \"" + transferMode + "\"}}";
            PostUrl = GetPostUrl() + "transfer";

            int fundTrandId = InsertFundTranferDetail(remittermobile, beneficiaryid, amount, transferMode, agentId, trackid, RemitterId);

            string response = Post("POST", PostUrl, reqJson, "Fund_Transfer", agentId, trackid);

            UpdateFundTranferDetail(fundTrandId, response);

            return response;
        }

        public static string GetRemitterRemingLimit(string remitterId, string agentId, string mobile)
        {
            return InstantPay_DataBase.GetRemitterRemingLimit(remitterId, agentId, mobile);
        }

        public static DataTable GetTransactionHistory(string mobile, string remitterId)
        {
            return InstantPay_DataBase.GetTransactionHistory(mobile, remitterId);
        }

        public static DataTable GetFilterTransactionHistory(string remitterId, string fromDate, string toDate, string trackId, string filstatus)
        {
            return InstantPay_DataBase.GetFilterTransactionHistory(remitterId, fromDate, toDate, trackId, filstatus);
        }

        public static bool ProcessToReFund(string transid, string reportid)
        {
            return InstantPay_DataBase.ProcessToReFund(transid, reportid);
        }

        //public static string GetBankDetails(string account, string agentId)
        //{
        //    string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"account\": \"" + account + "\",\"outletid\":" + OutletId + "}}";
        //    PostUrl = GetPostUrl() + "bank_details";

        //    return Post("POST", PostUrl, reqJson, "Get_Bank_Details", agentId, GetTrackId());
        //}

        public static bool BindBankDetails(string agentId)
        {
            try
            {
                string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"outletid\":" + OutletId + "}}";
                PostUrl = GetPostUrl() + "bank_details";

                string JsonResponse = Post("POST", PostUrl, reqJson, "GetBankDetail", agentId, GetTrackId());
                if (!string.IsNullOrEmpty(JsonResponse))
                {
                    dynamic dyResult = JObject.Parse(JsonResponse);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        BankDetail myDeserializedClass = JsonConvert.DeserializeObject<BankDetail>(JsonResponse);

                        return InstantPay_DataBase.BindBankDetails(myDeserializedClass.data);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static string Post(string postType, string Url, string ReqJson, string actionType, string agentId, string trackid)
        {
            string ReturnValue = string.Empty;
            InstantPay_DataBase.Information(Url, "Request captured in post Method : " + Url + "", ReqJson, "", actionType, agentId, trackid);

            StringBuilder sbResult = new StringBuilder();

            //ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(Url);

            try
            {
                Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");

                Http.Method = postType;
                byte[] lbPostBuffer = Encoding.UTF8.GetBytes(ReqJson);
                Http.ContentLength = lbPostBuffer.Length;
                Http.Timeout = 130000; //5000 milliseconds == 5 seconds// 900000 milliseconds == 900 seconds- 15 mints
                Http.ContentType = "application/json";
                Http.Accept = "application/json";

                using (Stream PostStream = Http.GetRequestStream())
                {
                    PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        ReturnValue = sbResult.ToString();
                        responseStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                InstantPay_DataBase.Error("Request in Post Method : " + Url + "", Url, ReqJson, "Response in Post Method : " + ReturnValue + "", "Exception in Post Method:" + ex.Message.Replace("'", "''") + "", actionType, agentId, trackid);
                ReturnValue = null;
            }
            finally
            {
                Http.Abort();
                Http = null;
            }
            InstantPay_DataBase.Information(Url, "Request captured in post Method : " + Url + "", ReqJson, ReturnValue, actionType, agentId, trackid);
            return ReturnValue;
        }
        #endregion

        #region [Data Base Section]

        private static bool DeleteBeneficiaryDetail(string benid, string remitterId, string agentId, string response)
        {
            try
            {
                if (!string.IsNullOrEmpty(response))
                {
                    dynamic dyResult = JObject.Parse(response);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        return InstantPay_DataBase.DeleteBeneficiaryDetail(benid, remitterId, agentId);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        private static bool UpdateRemitterRegistration(int remtID, string resultRespo, string agentId)
        {
            try
            {
                if (!string.IsNullOrEmpty(resultRespo))
                {
                    dynamic dyResult = JObject.Parse(resultRespo);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "otp sent successfully")
                    {
                        dynamic dyData = dyResult.data;
                        dynamic remitter = dyData.remitter;

                        string remitterId = remitter.id;
                        string verifiedStatus = remitter.is_verified;

                        if (!InstantPay_DataBase.CheckRemitterExist(remitterId))
                        {
                            return InstantPay_DataBase.UpdateRemitterRegistration(remtID, remitterId, verifiedStatus, agentId);
                        }
                        else
                        {
                            //return InstantPay_DataBase.DeleteRemitter(remtID);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        private static bool UpdateRemitterRegistrationValidate(string response, string remitterid, string mobile, string agentId)
        {
            try
            {
                if (!string.IsNullOrEmpty(response))
                {
                    dynamic dyResult = JObject.Parse(response);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        dynamic dyData = dyResult.data;
                        dynamic remitter = dyData.remitter;

                        string remitterId = remitter.id;
                        string verifiedStatus = remitter.is_verified;

                        return InstantPay_DataBase.UpdateRemitterRegistrationValidate(remitterId, mobile, verifiedStatus, agentId);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        private static bool UpdateInstantPayRemitterRegResponse(string remtRespo, string agentId)
        {
            try
            {
                if (!string.IsNullOrEmpty(remtRespo))
                {
                    RemitterRegResponse respo = new RemitterRegResponse();
                    bool executeUpdate = false;

                    dynamic dyResult = JObject.Parse(remtRespo);
                    dynamic dyData = dyResult.data;
                    dynamic remitter = dyData.remitter;

                    dynamic remitter_limit = dyData.remitter_limit[0];
                    dynamic remitter_mode = remitter_limit.mode;
                    dynamic remitter_limit_limit = remitter_limit.limit;

                    //string imps = ""; string neft = ""; decimal creditLtm = 0; decimal consumedAmount = 0; decimal remainningAmount = 0;
                    //foreach (var item in remitter_limit.mode[0])
                    //{
                    //    imps = item.imps;
                    //    neft = item.neft;
                    //}
                    //foreach (var item in remitter_limit.limit)
                    //{
                    //    creditLtm = creditLtm + Convert.ToDecimal(item.totalAmount);
                    //    consumedAmount = consumedAmount + Convert.ToDecimal(item.consumedAmount);
                    //    remainningAmount = remainningAmount + Convert.ToDecimal(item.remainningAmount);
                    //}

                    respo.RemitterId = remitter.id;
                    respo.Address = remitter.address;
                    respo.City = remitter.city;
                    respo.IsVerified = remitter.is_verified;
                    respo.KYCDoc = remitter.kycdocs;
                    respo.KYCStatus = remitter.kycstatus;
                    respo.Mobile = remitter.mobile;
                    respo.Name = remitter.name;
                    respo.PernTxnLimit = remitter.perm_txn_limit;
                    respo.PinCode = remitter.pincode;
                    respo.State = remitter.state;
                    respo.CreditLimit = remitter_limit_limit.total;
                    respo.ConsumedAmount = remitter_limit_limit.consumed;
                    respo.RemainingLimit = remitter_limit_limit.remaining;
                    respo.IMPSMode = remitter_mode.imps;
                    respo.NEFTMode = remitter_mode.neft;
                    //respo.CreditLimit = creditLtm.ToString();
                    //respo.ConsumedAmount = consumedAmount.ToString();
                    //respo.RemainingLimit = remainningAmount.ToString();
                    //respo.IMPSMode = imps;
                    //respo.NEFTMode = neft;

                    if (InstantPay_DataBase.UpdateRemitterRegResponse(respo))
                    {
                        return true;
                    }
                    //DataTable dtRemttDetail = InstantPay_DataBase.GetReitterResponseDetails(respo.RemitterId);
                    //if (dtRemttDetail != null && dtRemttDetail.Rows.Count > 0)
                    //{
                    //    string address = dtRemttDetail.Rows[0]["Address"].ToString();
                    //    string pincode = dtRemttDetail.Rows[0]["PinCode"].ToString();

                    //    if (string.IsNullOrEmpty(address) && string.IsNullOrEmpty(pincode))
                    //    {
                    //        executeUpdate = true;
                    //    }
                    //}

                    //if (executeUpdate)
                    //{
                    //    if (InstantPay_DataBase.UpdateRemitterRegResponse(respo))
                    //    {
                    //        return true;
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static int InsertFundTranferDetail(string remittermobile, string beneficiaryid, string amount, string transferMode, string agentId, string trackid, string RemitterId)
        {
            try
            {
                return InstantPay_DataBase.InsertT_InstantPayFundTransfer(remittermobile, beneficiaryid, amount, transferMode, agentId, trackid, RemitterId);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 0;
        }

        public static bool UpdateFundTranferDetail(int fundTrandId, string response)
        {
            try
            {
                if (!string.IsNullOrEmpty(response))
                {
                    dynamic dyResult = JObject.Parse(response);
                    dynamic dyData = dyResult.data;
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    FundTransfer fund = new FundTransfer();

                    fund.FundTransId = fundTrandId;
                    if (statusCode.ToLower() != "dtx" && statusMessage.ToLower() != "duplicate transaction")
                    {
                        if (statusCode.ToLower() == "err" && statusMessage.ToLower() == "transaction failed")
                        {
                            fund.RefundId = "RUF" + fundTrandId + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10).ToUpper();
                        }
                        else
                        {
                            fund.RefundId = string.Empty;
                        }
                        fund.ipay_id = dyData.ipay_id;
                        fund.ref_no = dyData.ref_no;
                        fund.opr_id = dyData.opr_id;
                        fund.name = dyData.name;
                        fund.opening_bal = dyData.opening_bal;
                        fund.charged_amt = dyData.charged_amt;
                        fund.locked_amt = dyData.locked_amt;
                        fund.ccf_bank = dyData.ccf_bank;
                        fund.bank_alias = dyData.bank_alias;
                    }
                    else
                    {
                        fund.RefundId = "RUF" + fundTrandId + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10).ToUpper();
                    }
                    fund.timestamp = dyResult.timestamp;
                    fund.ipay_uuid = dyResult.ipay_uuid;
                    fund.orderid = dyResult.orderid;
                    fund.environment = dyResult.environment;

                    fund.Status = statusMessage;

                    return InstantPay_DataBase.UpdateFundTranferDetail(fund);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool InsertBeneficiaryDetails(string remitterId, string agentId, string remittermobile, dynamic beneficiary)
        {
            bool isSuccess = false;

            try
            {
                List<string> benids = new List<string>();

                if (beneficiary != null)
                {
                    DataTable dtBenDetails = GetBenDetailsByRemitterId(remitterId, agentId);
                    if (dtBenDetails != null && dtBenDetails.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtBenDetails.Rows.Count; i++)
                        {
                            benids.Add(dtBenDetails.Rows[i]["BeneficiaryId"].ToString().Trim());
                        }
                    }

                    //bool isDeleted = InstantPay_DataBase.DeleteBeneficiaryByMobile(remittermobile, agentId, remitterId);

                    foreach (var item in beneficiary)
                    {
                        RemitterBenDetail model = new RemitterBenDetail();

                        model.BeneficiaryId = item.id;
                        if (!benids.Contains(model.BeneficiaryId))
                        {
                            model.Name = item.name;
                            model.Mobile = item.mobile;
                            model.Account = item.account;
                            model.IfscCode = item.ifsc;
                            model.RemitterId = remitterId;
                            model.AgentId = agentId;
                            model.Status = item.status;
                            model.Bank = item.bank;
                            model.imps = item.imps;
                            model.lastSuccessDate = item.last_success_date;
                            model.lastSuccessImps = item.last_success_imps;
                            model.lastSuccessName = item.last_success_name;

                            if (InstantPay_DataBase.InsertT_InstantPayBeneficaryDetails(model))
                            {
                                isSuccess = true;
                            }
                            else
                            {
                                isSuccess = false;
                            }
                        }
                        else
                        {
                            DataTable dtBen = InstantPay_DataBase.GetBenDetailsByRemitterId(remitterId, agentId, model.BeneficiaryId);
                            if (dtBen != null && dtBen.Rows.Count > 0)
                            {
                                string bankrespo = !string.IsNullOrEmpty(dtBen.Rows[0]["bank"].ToString()) ? dtBen.Rows[0]["bank"].ToString() : string.Empty;
                                if (string.IsNullOrEmpty(bankrespo))
                                {
                                    model.Name = item.name;
                                    model.Mobile = item.mobile;
                                    model.Account = item.account;
                                    model.IfscCode = item.ifsc;
                                    model.RemitterId = remitterId;
                                    model.AgentId = agentId;
                                    model.Status = item.status;
                                    model.Bank = item.bank;
                                    model.imps = item.imps;
                                    model.lastSuccessDate = item.last_success_date;
                                    model.lastSuccessImps = item.last_success_imps;
                                    model.lastSuccessName = item.last_success_name;

                                    if (InstantPay_DataBase.UpdateT_InstantPayBeneficaryDetails(model))
                                    {
                                        isSuccess = true;
                                    }
                                    else
                                    {
                                        isSuccess = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return isSuccess;
        }

        public static string GetFundTranferHistory(string remitterid, string agentId)
        {
            string result = string.Empty;

            try
            {

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public static bool IsRemitterExist(string remitterid, string mobile, string agentid)
        {
            try
            {
                DataTable dtRemitter = InstantPay_DataBase.GetReitterDetails(remitterid, mobile, agentid);

                if (dtRemitter != null && dtRemitter.Rows.Count > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool InsertRemitterDetails(string response, string agentId)
        {
            try
            {
                if (!string.IsNullOrEmpty(response))
                {
                    dynamic dyResult = JObject.Parse(response);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        dynamic dyData = dyResult.data;
                        dynamic remitter = dyData.remitter;

                        RemitterRegRequest remttRequest = new RemitterRegRequest();
                        remttRequest.Mobile = remitter.mobile;
                        remttRequest.FirstName = remitter.name;
                        remttRequest.LastName = string.Empty;
                        remttRequest.PinCode = remitter.pincode;
                        remttRequest.AgentUserId = agentId;
                        remttRequest.RemitterId = remitter.id;
                        remttRequest.Status = "already registered";
                        remttRequest.IsKYCStatus = remitter.kycstatus;
                        remttRequest.VerifiedStatus = remitter.is_verified;

                        if (InstantPay_DataBase.Insert_T_InstantPayRemitterReg(remttRequest))
                        {
                            dynamic remitter_limit = dyData.remitter_limit[0];
                            dynamic remitter_mode = remitter_limit.mode;
                            dynamic remitter_limit_limit = remitter_limit.limit;

                            //string imps = ""; string neft = ""; decimal creditLtm = 0; decimal consumedAmount = 0; decimal remainningAmount = 0;
                            //foreach (var item in remitter_mode)
                            //{
                            //    imps = item.imps;
                            //    neft = item.neft;
                            //}
                            //foreach (var item in remitter_limit.limit)
                            //{
                            //    creditLtm = creditLtm + (item["total"] != null ? Convert.ToDecimal(item.total) : (item["totalAmount"] != null ? Convert.ToDecimal(item.totalAmount) : 0));
                            //    consumedAmount = consumedAmount + Convert.ToDecimal(item.consumedAmount);
                            //    remainningAmount = remainningAmount + Convert.ToDecimal(item.remainningAmount);
                            //}

                            RemitterRegResponse remttResponse = new RemitterRegResponse();
                            remttResponse.RemitterId = remitter.id;
                            remttResponse.AgentUserId = agentId;
                            remttResponse.Address = remitter.address;
                            remttResponse.City = remitter.city;
                            remttResponse.IsVerified = remitter.is_verified;
                            remttResponse.KYCDoc = remitter.kycdocs;
                            remttResponse.KYCStatus = remitter.kycstatus;
                            remttResponse.Mobile = remitter.mobile;
                            remttResponse.Name = remitter.name;
                            remttResponse.PernTxnLimit = remitter.perm_txn_limit;
                            remttResponse.PinCode = remitter.pincode;
                            remttResponse.State = remitter.state;
                            remttResponse.CreditLimit = remitter_limit_limit.total;
                            remttResponse.ConsumedAmount = remitter_limit_limit.consumed;
                            remttResponse.RemainingLimit = remitter_limit_limit.remaining;
                            remttResponse.IMPSMode = remitter_mode.imps;
                            remttResponse.NEFTMode = remitter_mode.neft;
                            //remttResponse.CreditLimit = creditLtm.ToString();
                            //remttResponse.ConsumedAmount = consumedAmount.ToString();
                            //remttResponse.RemainingLimit = remainningAmount.ToString();
                            //remttResponse.IMPSMode = imps;
                            //remttResponse.NEFTMode = neft;

                            return InstantPay_DataBase.Insert_T_InstantPayRemitterRegResponse(remttResponse);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static DataTable GetT_InstantPayRemitterRegResponse(string agentid, string remitterid, string mobile)
        {
            DataTable dtRemitter = new DataTable();

            try
            {
                dtRemitter = InstantPay_DataBase.GetT_InstantPayRemitterDetail(agentid, remitterid, mobile);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dtRemitter;
        }

        private static int InsertT_InstantPayBeneficary(string remitterid, string name, string mobile, string account, string bankname, string ifsc, string agentId)
        {
            RemitterBenDetail ben = new RemitterBenDetail();
            ben.Name = name;
            ben.Mobile = mobile;
            ben.Account = account;
            ben.IfscCode = ifsc;
            ben.RemitterId = remitterid;
            ben.AgentId = agentId;
            ben.ReqBank = bankname;

            return InstantPay_DataBase.InsertT_InstantPayBeneficary(ben);
        }

        private static bool Update_T_InstantPayBeneficary(int benId, string response)
        {
            if (!string.IsNullOrEmpty(response))
            {
                dynamic dyResult = JObject.Parse(response);
                string statusCode = dyResult.statuscode;
                string statusMessage = dyResult.status;

                if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                {
                    dynamic dyData = dyResult.data;
                    dynamic remitter = dyData.remitter;
                    dynamic beneficiary = dyData.beneficiary;

                    RemitterBenDetail ben = new RemitterBenDetail();
                    ben.BenID = benId;
                    ben.RemitterId = remitter.id;
                    ben.BeneficiaryId = beneficiary.id;
                    ben.Status = beneficiary.status;

                    ben.TimeStamp = dyResult.timestamp;
                    ben.Ipay_uuid = dyResult.ipay_uuid;
                    ben.OrderId = dyResult.orderid;
                    ben.Environment = dyResult.environment;

                    return InstantPay_DataBase.Update_T_InstantPayBeneficary(ben);
                }
            }

            return false;
        }

        public static DataTable GetBenDetailsByRemitterId(string remitterId, string agentId, string beneficaryId = null)
        {
            return InstantPay_DataBase.GetBenDetailsByRemitterId(remitterId, agentId, beneficaryId);
        }

        public static DataTable GetFundTransferVeryficationDetail(string remitterId, string beneficaryId, ref DataTable bankdetail)
        {
            return InstantPay_DataBase.GetFundTransferVeryficationDetail(remitterId, beneficaryId, ref bankdetail);
        }
        #endregion

        #region [Common]
        public static string Encrypt(string value)
        {
            //byte[] keyArray;
            //byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(value);
            //string key = "dmt";
            //MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            //keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            //hashmd5.Clear();
            //TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //tdes.Key = keyArray;
            //tdes.Mode = CipherMode.ECB;
            //tdes.Padding = PaddingMode.PKCS7;
            //ICryptoTransform ctransform = tdes.CreateEncryptor();
            //byte[] resultArray = ctransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            //tdes.Clear();
            //return Convert.ToBase64String(resultArray, 0, resultArray.Length);

            string encData = null;
            byte[][] keys = GetHashKeys("dmt");

            try
            {
                encData = EncryptStringToBytes_Aes(value, keys[0], keys[1]);
            }
            catch (CryptographicException)
            {
            }
            catch (ArgumentNullException)
            {

            }

            return encData;
        }

        private static byte[][] GetHashKeys(string key)
        {
            byte[][] result = new byte[2][];
            Encoding enc = Encoding.UTF8;

            SHA256 sha2 = new SHA256CryptoServiceProvider();
            int a = 5, b = 6, c = -7, d = -8, f = 4, e = 12;
            int Formula_one = ((a * b) + (c + f) / (d + e));
            string Result = Formula_one.ToString();
            string Raw_key = Result + key;
            byte[] rawKey = enc.GetBytes(Raw_key);



            string Row_IV = Result + key;
            byte[] rawIV = enc.GetBytes(Row_IV);


            string result_byte = Result;
            byte[] bytes = Encoding.ASCII.GetBytes(result_byte);
            byte[] Hash_Key = Combine(bytes, rawKey);
            byte[] Combine(byte[] a1, byte[] a2)
            {
                byte[] ret = new byte[a1.Length + a2.Length];
                Array.Copy(a1, 0, ret, 0, a1.Length);
                Array.Copy(a2, 0, ret, a1.Length, a2.Length);
                return ret;
            }

            byte[] hashKey = sha2.ComputeHash(Hash_Key);


            byte[] bytess = Encoding.ASCII.GetBytes(result_byte);
            byte[] Hash_IV = CombineTime(bytess, rawIV);
            byte[] CombineTime(byte[] a1, byte[] a2)
            {
                byte[] ret = new byte[a1.Length + a2.Length];
                Array.Copy(a1, 0, ret, 0, a1.Length);
                Array.Copy(a2, 0, ret, a1.Length, a2.Length);
                return ret;
            }
            byte[] hashIV = sha2.ComputeHash(Hash_IV);

            Array.Resize(ref hashIV, 16);

            result[0] = hashKey;
            result[1] = hashIV;

            return result;
        }
        private static string EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            byte[] encrypted;

            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt =
                            new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            return Convert.ToBase64String(encrypted);
        }

        public static string Decrypt(string value)
        {
            //byte[] keyArray;
            //byte[] toEncryptArray = Convert.FromBase64String(value);
            //string key = "dmt";
            //MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            //keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            //hashmd5.Clear();
            //TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //tdes.Key = keyArray;
            //tdes.Mode = CipherMode.ECB;
            //tdes.Padding = PaddingMode.PKCS7;
            //ICryptoTransform ctransform = tdes.CreateDecryptor();
            //byte[] resultArray = ctransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            //tdes.Clear();
            //return UTF8Encoding.UTF8.GetString(resultArray);
            string decData = null;
            byte[][] keys = GetHashKeys("dmt");

            try
            {
                decData = DecryptStringFromBytes_Aes(value, keys[0], keys[1]);
            }
            catch (CryptographicException) { }
            catch (ArgumentNullException) { }

            return decData;
        }

        public static string DecryptStringFromBytes_Aes(string cipherTextString, byte[] Key, byte[] IV)
        {

            byte[] cipherText = Convert.FromBase64String(cipherTextString);

            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            string plaintext = null;

            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt =
                            new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }
        #endregion

        public static DataTable GetCombieRemitterDetail(string userId, string remitterId, string mobile)
        {
            return InstantPay_DataBase.GetCombieRemitterDetail(userId, remitterId, mobile);
        }

        public static DataTable GetTransactionHistoryByTrackId(string trackid)
        {
            return InstantPay_DataBase.GetTransactionHistoryByTrackId(trackid);
        }

        public static DataTable GetAgencyDetailById(string agencyId)
        {
            return InstantPay_DataBase.GetAgencyDetailById(agencyId);
        }

        public static DataTable GetTopBindAllBank()
        {
            return InstantPay_DataBase.GetTopBindAllBank();
        }

        public static DataTable GetFurtherBindAllBank()
        {
            return InstantPay_DataBase.GetFurtherBindAllBank();
        }

        public static bool UpdateLocalAddress(string remitterId, string agentId, string mobile, string address)
        {
            return InstantPay_DataBase.UpdateLocalAddress(remitterId, agentId, mobile, address);
        }

        #region [Get Latitude and Longnitude]
        public static List<string> GetLatitudeLongnitude(string address)
        {
            List<string> result = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(address))
                {
                    var locationService = new GoogleLocationService();
                    var point = locationService.GetLatLongFromAddress(address);

                    var latitude = point.Latitude;
                    var longitude = point.Longitude;

                    result.Add(latitude.ToString());
                    result.Add(longitude.ToString());
                }

                string url = "http://maps.google.com/maps/api/geocode/xml?address=" + address + "&sensor=false&key=AIzaSyAvwnzh2TvYbh_cTZgKm6XzX2CDDeQJW9A";
                WebRequest request = WebRequest.Create(url);

                using (WebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        DataSet dsResult = new DataSet();
                        dsResult.ReadXml(reader);
                        DataTable dtCoordinates = new DataTable();
                        dtCoordinates.Columns.AddRange(new DataColumn[4] { new DataColumn("Id", typeof(int)),
                    new DataColumn("Address", typeof(string)),
                    new DataColumn("Latitude",typeof(string)),
                    new DataColumn("Longitude",typeof(string)) });
                        foreach (DataRow row in dsResult.Tables["result"].Rows)
                        {
                            string geometry_id = dsResult.Tables["geometry"].Select("result_id = " + row["result_id"].ToString())[0]["geometry_id"].ToString();
                            DataRow location = dsResult.Tables["location"].Select("geometry_id = " + geometry_id)[0];
                            dtCoordinates.Rows.Add(row["result_id"], row["formatted_address"], location["lat"], location["lng"]);
                        }
                    }
                    // return dtCoordinates;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        #endregion

        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    string ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        //    if (string.IsNullOrEmpty(ipAddress))
        //    {
        //        ipAddress = Request.ServerVariables["REMOTE_ADDR"];
        //    }

        //    string APIKey = "<Your API Key>";
        //    string url = string.Format("http://api.ipinfodb.com/v3/ip-city/?key={0}&ip={1}&format=json", APIKey, ipAddress);
        //    using (WebClient client = new WebClient())
        //    {
        //        string json = client.DownloadString(url);
        //        Location location = new JavaScriptSerializer().Deserialize<Location>(json);
        //        List<Location> locations = new List<Location>();
        //        locations.Add(location);
        //        gvLocation.DataSource = locations;
        //        gvLocation.DataBind();
        //    }
        //}

        public static int InsertInitiatePayout(InitiatePayout payout)
        {
            try
            {
                return InstantPay_DataBase.InsertInitiatePayout(payout);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 0;
        }

        public static string InitiatePayout(string mobile, string accountno, string transtype, string ifsccode, string latitude, string longitude, string ipaddress, string alert_email, string agentId, string RemitterId)
        {
            string response = string.Empty;

            try
            {
                string clientRefId = GetTrackId();
                string transfermode = transtype;

                if (transtype.ToLower() == "imps") { transtype = "DPN"; }
                else if (transtype.ToLower() == "neft") { transtype = "BPN"; }
                else { transtype = "CPN"; }

                string remarks = "Check_Beneficiary_Name";

                string reqJson = "{\"token\": \"" + TokenID + "\",\"request\":{\"sp_key\":\"" + transtype + "\",\"external_ref\":\"" + clientRefId + "\",\"credit_account\":\"" + accountno + "\",\"ifs_code\":\"" + ifsccode + "\","
                    + "\"bene_name\":\"none\",\"credit_amount\":\"1\",\"latitude\":\"" + latitude + "\",\"longitude\":\"" + longitude + "\",\"endpoint_ip\":\"" + ipaddress + "\",\"alert_mobile\":\"" + mobile + "\",\"alert_email\":\"" + alert_email + "\","
                    + "\"remarks\":\"" + remarks + "\"}}";

                PostUrl = GetInitiatePayoutPostUrl() + "direct";

                InitiatePayout payout = new InitiatePayout();
                payout.AgentId = agentId;
                payout.RemitterMobile = mobile;
                payout.sp_key = transtype;
                payout.bene_name = "none";
                payout.credit_amount = "1";
                payout.latitude = latitude;
                payout.longitude = longitude;
                payout.endpoint_ip = ipaddress;
                payout.alert_mobile = mobile;
                payout.alert_email = "";
                payout.otp_auth = "";
                payout.otp = "";
                payout.remarks = remarks;
                payout.TrackId = clientRefId;
                payout.RemitterId = RemitterId;

                int payoutId = InsertInitiatePayout(payout);

                response = Post("POST", PostUrl, reqJson, "direct", agentId, clientRefId);
                //response = "{'statuscode': 'TXN','status': 'Transaction Successful','data': {'external_ref': 'RTFRT5465HFGHG','ipay_id': '1200825120941JNLSB','transfer_value': '1.00','type_pricing': 'CHARGE','commercial_value': '1.1800','value_tds': '0.0000',        'ccf': '0.00','vendor_ccf': '0.00','charged_amt': '2.18','payout': {'credit_refid': '023812469633','account': '04362191032205','ifsc': 'ORBC0100436','name': 'KRISHNA KANT SO JINI'}},'timestamp': '2020-08-25 12:09:42','ipay_uuid': 'F76E0CF70C5563A54A7A','orderid': '1200825120941JNLSB','environment': 'PRODUCTION'}";

                if (!string.IsNullOrEmpty(response))
                {
                    dynamic dyResult = JObject.Parse(response);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        dynamic dyData = dyResult.data;
                        dynamic dypayout = dyData.payout;

                        InitiatePayout uppayout = new InitiatePayout();

                        uppayout.InitiatePayoutId = payoutId;
                        uppayout.external_ref = dyData.external_ref;
                        uppayout.ipay_id = dyData.ipay_id;
                        uppayout.transfer_value = dyData.transfer_value;
                        uppayout.type_pricing = dyData.type_pricing;
                        uppayout.commercial_value = dyData.commercial_value;
                        uppayout.value_tds = dyData.value_tds;
                        uppayout.ccf = dyData.ccf;
                        uppayout.vendor_ccf = dyData.vendor_ccf;
                        uppayout.charged_amt = dyData.charged_amt;
                        uppayout.payout_credit_refid = dypayout.credit_refid;
                        uppayout.payout_account = dypayout.account;
                        uppayout.payout_ifsc = dypayout.ifsc;
                        uppayout.payout_name = dypayout.name;
                        uppayout.timestamp = dyResult.timestamp;
                        uppayout.ipay_uuid = dyResult.ipay_uuid;
                        uppayout.orderid = dyResult.orderid;
                        uppayout.environment = dyResult.environment;
                        uppayout.Status = statusMessage;

                        bool isUpdated = InstantPay_DataBase.UpdateInitiatePayout(uppayout);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return response;
        }

        //==================================New Section=========================================================       
        private static bool UpdateRemitterRegDetail(int remtID, string mobile, string agentId, string resultRespo)
        {
            try
            {
                if (!string.IsNullOrEmpty(resultRespo))
                {
                    dynamic dyResult = JObject.Parse(resultRespo);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "otp sent successfully")
                    {
                        dynamic dyData = dyResult.data;
                        dynamic remitter = dyData.remitter;

                        string remitterId = remitter.id;
                        string verifiedStatus = remitter.is_verified;

                        return InstantPay_DataBase.UpdateRemitterOTPDetail(remtID, mobile, agentId, remitterId);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static string CheckRemitterValidate(string remitterid, string mobile, string otp, string agentId)
        {
            string result = string.Empty;

            string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"remitterid\": \"" + remitterid + "\",\"mobile\": \"" + mobile + "\",\"otp\": \"" + otp + "\",\"outletid\":" + OutletId + "}}";
            PostUrl = GetPostUrl() + "remitter_validate";

            result = Post("POST", PostUrl, reqJson, "Remitter_Registration_Validate", agentId, GetTrackId());
            if (!string.IsNullOrEmpty(result))
            {
                dynamic dyOtpResult = JObject.Parse(result);
                string otpstatusCode = dyOtpResult.statuscode;
                string otpstatusMessage = dyOtpResult.status;

                if (otpstatusCode.ToLower().Trim() == "txn" && otpstatusMessage.ToLower().Trim() == "otp sent successfully")
                {
                    string remitterJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"mobile\": \"" + mobile + "\",\"outletid\":" + OutletId + "}}";
                    PostUrl = GetPostUrl() + "remitter_details";

                    string remitterResult = Post("POST", PostUrl, remitterJson, "GetRemitterDetail", agentId, GetTrackId());
                    if (!string.IsNullOrEmpty(remitterResult))
                    {
                        dynamic dyResult = JObject.Parse(remitterResult);
                        string statusCode = dyResult.statuscode;
                        string statusMessage = dyResult.status;

                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                        {
                            dynamic dyData = dyResult.data;
                            dynamic remitter = dyData.remitter;
                            dynamic beneficiary = dyData.beneficiary;

                            string remt_id = remitter.id;
                            string name = remitter.name;
                            string respo_mobile = remitter.mobile;
                            string address = remitter.address;
                            string respo_pincode = remitter.pincode;
                            string city = remitter.city;
                            string state = remitter.state;
                            string kycstatus = remitter.kycstatus;
                            string consumedlimit = remitter.consumedlimit;
                            string remaininglimit = remitter.remaininglimit;
                            string kycdocs = remitter.kycdocs;
                            string is_verified = remitter.is_verified;
                            string perm_txn_limit = remitter.perm_txn_limit;

                            bool isSuccess = InstantPay_DataBase.Update_T_InstantPayRemitterDetailResponse(remt_id, name, respo_mobile, address, respo_pincode, city, state, kycstatus, consumedlimit, remaininglimit, kycdocs, is_verified, perm_txn_limit, agentId);
                        }
                    }
                }
            }

            return result;
        }

        public static bool InsertAlreadyRegRemitterDetails(string response, string agentId)
        {
            try
            {
                if (!string.IsNullOrEmpty(response))
                {
                    dynamic dyResult = JObject.Parse(response);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        dynamic dyData = dyResult.data;
                        dynamic remitter = dyData.remitter;

                        dynamic remitter_limit = dyData.remitter_limit[0];
                        dynamic remitter_limit_limit = remitter_limit.limit;

                        string RegMobile = remitter.mobile;
                        string FirstName = remitter.name;
                        string PinCode = remitter.pincode;
                        string CurrentAddress = remitter.address;
                        string AgentId = agentId;
                        string id = remitter.id;
                        string name = remitter.name;
                        string mobile = remitter.mobile;
                        string address = remitter.address;
                        string pincode_res = remitter.pincode;
                        string city = remitter.city;
                        string state = remitter.state;
                        string kycstatus = remitter.kycstatus;
                        string consumedlimit = remitter.consumedlimit;
                        string remaininglimit = remitter.remaininglimit;
                        string kycdocs = remitter.kycdocs;
                        string is_verified = remitter.is_verified;
                        string perm_txn_limit = remitter.perm_txn_limit;
                        string CreditLimit = remitter_limit_limit.total;

                        string query = "insert into T_InstantPayRemitterDetail "
                            + "(RegMobile,FirstName,LastName,PinCode,CurrentAddress,AgentId,id,name,mobile,address,pincode_res,city,state,kycstatus,consumedlimit,remaininglimit,kycdocs,is_verified,perm_txn_limit,CreditLimit) values "
                            + "('" + RegMobile + "','" + FirstName + "','','" + PinCode + "','" + CurrentAddress + "','" + AgentId + "','" + id + "','" + name + "',"
                            + "'" + mobile + "','" + address + "','" + pincode_res + "','" + city + "','" + state + "','" + kycstatus + "','" + consumedlimit + "','" + remaininglimit + "',"
                            + "'" + kycdocs + "','" + is_verified + "','" + perm_txn_limit + "','" + CreditLimit + "')";

                        bool isSuccess = InstantPay_DataBase.Insert_AlreadyRegisteredRemitterDetails(query);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
    }
}
