﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstantPayServiceLib
{
    public class InstantPayModel
    {
        public class IPDMTRemitterDetails
        {
            public string Id { get; set; }
            public string Mobile { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string PinCode { get; set; }
            public string Mode { get; set; }
            public string AgentId { get; set; }
            public string RemitterId { get; set; }
            public string Status { get; set; }
            public bool IsKYCStatus { get; set; }
        }

        public class IPDMTFundTransfer
        {
            public string FundTransId { get; set; }
            public string AgentId { get; set; }
            public string RemitterMobile { get; set; }
            public string BenificieryId { get; set; }
            public string Amount { get; set; }
            public string TxnMode { get; set; }
            public string IpayReferenceId { get; set; }
            public string IpayRefNumber { get; set; }
            public string IpayOprId { get; set; }
            public string TransactionId { get; set; }
            public string FundTransStatus { get; set; }
            public string Merchant { get; set; }
            public string Mode { get; set; }
            public string CreatedDate { get; set; }
            public string TrackId { get; set; }
        }

    }
}
