﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using static InstantPayServiceLib.InstantPayModel;

namespace InstantPayServiceLib
{
    public static class InstantPayFun
    {
        public static List<string> GetRemitterDetail(string mobile, string agentid, string mode)
        {
            //InstantPayLib.Dmt.PaymentMain objDmtApi = new InstantPayLib.Dmt.PaymentMain();
            //DMT.Dmt.Model.RemitterDetailRequest obkjRD = new DMT.Dmt.Model.RemitterDetailRequest();
            //obkjRD.Mobile = mobile;
            //obkjRD.Mode = mode;
            //obkjRD.AgentCode = agentid;
            //DMT.Dmt.Model.RemitterDetailResponse objRD = new DMT.Dmt.Model.RemitterDetailResponse();
            //objRD=objDmtApi.RemitterDetail(obkjRD)


            string inputStr = "{\"Mobile\" :\"" + mobile + "\",\"AgentCode\" :\"" + agentid + "\",\"Mode\" :\"" + mode + "\"}";
            List<string> result = CommonRemitterPost("RemitterDetail", inputStr, "Get_Remitter_Detail");

            if (result != null)
            {
                if (result[0] == "success")
                {
                    bool isAdded = InstantPayDataBaseService.IPDMTRemitterRegResponseLog(mobile, agentid, result[1]);
                }
            }

            return result;
        }

        public static List<string> RemitterRegistration(string mobile, string firstName, string lastName, string pinCode, string agentid, string mode)
        {
            List<string> remRegDetail = new List<string>();
            try
            {
                string inputStr = "{\"Mobile\" :\"" + mobile + "\",\"FirstName\" :\"" + firstName + "\",\"LastName\" :\"" + lastName + "\",\"PinCode\" :\"" + pinCode + "\",\"Mode\" :\"" + mode + "\",\"AgentCode\" :\"" + agentid + "\"}";

                IPDMTRemitterDetails remDel = InstantPayDataBaseService.IPDMTGetRemitterDetails(mobile);

                int remitterInsId = 0;
                if (remDel.RemitterId == null)
                {
                    remitterInsId = InstantPayDataBaseService.IPDMTInsertRemitterDetails(mobile, firstName, lastName, pinCode, agentid, mode);
                }
                else
                {
                    remitterInsId = Convert.ToInt32(remDel.Id);
                }

                if (remitterInsId > 0)
                {
                    remRegDetail = CommonRemitterPost("RemitterRegistration", inputStr, "Remitter_Registration");

                    dynamic regDetails = JObject.Parse(remRegDetail[1]);
                    string statusCode = regDetails.statusCode;
                    string statusMessage = regDetails.statusMessage;

                    if (statusCode == "0" && statusMessage.ToLower().Trim() == "otp sent successfully")
                    {
                        string remiiterId = regDetails.data[0].remiiterId;
                        string verifiedStatus = regDetails.data[0].verifiedStatus;
                        bool isUpdatedRemitter = InstantPayDataBaseService.IPDMTUpdateRemitterDetails(remitterInsId, remiiterId, "registered", verifiedStatus);
                    }
                    else if (statusCode == "1" && (statusMessage.ToLower().Trim() == "please register with correct remitter name" || statusMessage.ToLower().Trim() == "invalid mobile number"))
                    {
                        bool isUpdatedRemitter = InstantPayDataBaseService.IPDMTDeleteRemitterDetails(remitterInsId);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return remRegDetail;
        }

        public static List<string> RemitterValidate(string remitterId, string mobile, string otp, string agentid, string mode)
        {
            List<string> result = new List<string>();
            try
            {
                string inputStr = "{\"RemitterId\" :\"" + remitterId + "\",\"Mobile\" :\"" + mobile + "\",\"Otp\" :\"" + otp + "\",\"Mode\" :\"" + mode + "\",\"AgentCode\" :\"" + agentid + "\"}";

                IPDMTRemitterDetails remDel = InstantPayDataBaseService.IPDMTGetRemitterDetails(mobile);
                if (remDel != null && !string.IsNullOrEmpty(remDel.RemitterId))
                {
                    if (remitterId == remDel.RemitterId)
                    {
                        result = CommonRemitterPost("RemitterValidate", inputStr, "Remitter_Validate");
                        if (result[0] == "success")
                        {
                            dynamic regDetails = JObject.Parse(result[1]);

                            string statusCode = regDetails.statusCode;
                            string statusMessage = regDetails.statusMessage;

                            if (statusCode == "0" && statusMessage.ToLower().Trim() == "transaction successful")
                            {
                                string remiiterId = regDetails.data[0].remiiterId;
                                string verifiedStatus = regDetails.data[0].verifiedStatus;

                                bool isUpdatedRemitter = InstantPayDataBaseService.IPDMTUpdateRemitterDetails(Convert.ToInt32(remDel.Id), remiiterId, "registered", verifiedStatus);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public static List<string> IPDMTInsertBeneficiary(string accountNumber, string ifscCode, string name, string mobile, string remitterId, string agentId, string merchant, string mode)
        {
            List<string> result = new List<string>();

            try
            {
                bool isBenAdded = InstantPayDataBaseService.IPDMTInsertBeneficiary(accountNumber, ifscCode, name, mobile, remitterId, agentId, merchant, mode);
                if (isBenAdded)
                {
                    string inputStr = "{\"AccountNumber\" :\"" + accountNumber + "\",\"IfscCode\" :\"" + ifscCode + "\",\"Name\" :\"" + name + "\",\"Mobile\" :\"" + mobile + "\",\"RemitterId\" :\"" + remitterId + "\",\"AgentCode\" :\"" + agentId + "\",\"Merchant\" :\"" + merchant + "\",\"Mode\" :\"" + mode + "\"}";

                    result = CommonRemitterPost("AddBeneficiary", inputStr, "Add_Beneficiary");

                    // List<string> ben = CommonRemitterPost("AddBeneficiary", inputStr);

                    //dynamic benDetails = JObject.Parse(ben[1]);
                    //string statusCode = benDetails.statusCode;
                    //string statusMessage = benDetails.statusMessage;
                    //if (statusCode == "0" && statusMessage.ToLower().Trim() == "transaction successful")
                    //{
                    //}
                }
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        public static List<string> IPDMTBenificiaryDeleteDetails(string remitterId, string benificiaryId, string agentId, string mode)
        {
            List<string> result = new List<string>();

            try
            {
                string inputStr = "{\"RemitterId\" :\"" + remitterId + "\",\"BenificiaryId\" :\"" + benificiaryId + "\",\"AgentCode\" :\"" + agentId + "\",\"Mode\" :\"" + mode + "\"}";

                result = CommonRemitterPost("BenificiaryDelete", inputStr, "Benificiary_Delete");

                // List<string> ben = CommonRemitterPost("AddBeneficiary", inputStr);

                //dynamic benDetails = JObject.Parse(ben[1]);
                //string statusCode = benDetails.statusCode;
                //string statusMessage = benDetails.statusMessage;
                //if (statusCode == "0" && statusMessage.ToLower().Trim() == "transaction successful")
                //{
                //}
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        public static List<string> IPDMTBenificiaryDeleteVarification(string remitterId, string benificiaryId, string otp, string agentId, string mode)
        {
            List<string> result = new List<string>();

            try
            {
                //string inputStr = "{\"RemitterId\" :\"" + remitterId + "\",\"BenificiaryId\" :\"" + benificiaryId + "\",\"AgentCode\" :\"" + agentId + "\",\"Otp\" :\"" + otp + "\",\"Mode\" :\"" + mode + "\"}";
                string inputStr = "{\"RemitterId\": \"" + remitterId + "\",\"BenificieryId\": \"" + benificiaryId + "\",\"AgentCode\": \"" + agentId + "\",\"Otp\": \"" + otp + "\",\"Mode\": \"" + mode + "\"}";

                result = CommonRemitterPost("BeniDeleteValidate", inputStr, "Benificiery_Delete_Validate");

                // List<string> ben = CommonRemitterPost("AddBeneficiary", inputStr);

                //dynamic benDetails = JObject.Parse(ben[1]);
                //string statusCode = benDetails.statusCode;
                //string statusMessage = benDetails.statusMessage;
                //if (statusCode == "0" && statusMessage.ToLower().Trim() == "transaction successful")
                //{
                //}
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        public static List<string> IPDMTMoneyTransfer(string remtmobile, string benid, string amount, string agentid, string txnmode, string merchant, string mode, string trackid)
        {
            List<string> result = new List<string>();

            try
            {
                //string inputStr = "{\"token\":\"3bb33ee981b2498afaa326a560a93d6d\",\"request\":{\"remittermobile\":\"" + mobile + "\",\"beneficiaryid\":\"" + benid + "\",\"agentid\":\"" + agentid + "\",\"amount\":\"" + amount + "\",\"mode\":\"" + txnmode + "\"}}";
                string inputStr = "{\"AgentCode\": \"" + agentid + "\",\"Merchant\": \"" + merchant + "\",\"Mode\": \"" + mode + "\",\"RemitterMobile\": \"" + remtmobile + "\",\"BenificieryId\": \"" + benid + "\",\"Amount\": \"" + amount + "\",\"TxnMode\": \"" + txnmode + "\"}";

                result = CommonRemitterPost("Transfer", inputStr, "Fund_Transfer", trackid);

                if (result != null && result.Count > 0)
                {
                    dynamic regDetails = JObject.Parse(result[1]);

                    string statusCode = regDetails.statusCode;
                    string statusMessage = regDetails.statusMessage;

                    //if (statusCode.ToLower() == "0" && statusMessage.ToLower().Trim() == "transaction successful")
                    //{
                    dynamic fundrespo = regDetails.response[0];

                    IPDMTFundTransfer fund = new IPDMTFundTransfer();

                    fund.AgentId = agentid;
                    fund.RemitterMobile = remtmobile;
                    fund.BenificieryId = benid;
                    fund.Amount = amount;
                    fund.TxnMode = txnmode;
                    fund.IpayReferenceId = fundrespo.ipayReferenceId != null ? fundrespo.ipayReferenceId : "";
                    fund.IpayRefNumber = fundrespo.ipayRefNumber != null ? fundrespo.ipayRefNumber : "";
                    fund.IpayOprId = fundrespo.ipayOprId != null ? fundrespo.ipayOprId : "";
                    fund.TransactionId = fundrespo.transactionId != null ? fundrespo.transactionId : "";
                    fund.FundTransStatus = fundrespo.status != null ? fundrespo.status : "";
                    fund.Merchant = merchant;
                    fund.Mode = mode;
                    fund.TrackId = trackid;

                    bool isInserted = InstantPayDataBaseService.InsertFundTransDetails(fund);
                    //}
                }
            }
            catch (Exception)
            {

                throw;
            }

            return result;
        }

        public static string GetRemmiterMobileByRemId(string remitterId)
        {
            return InstantPayDataBaseService.GetRemmiterMobileByRemId(remitterId);
        }

        public static DataTable GetMoneyTransferDetailsbyAgentId(string agentId, string remitterMobile)
        {
            return InstantPayDataBaseService.GetMoneyTransferDetailsbyAgentId(agentId, remitterMobile);
        }

        public static DataTable GetAgencyDetailById(string agencyId)
        {
            return InstantPayDataBaseService.GetAgencyDetailById(agencyId);
        }

        public static List<string> CommonRemitterPost(string url, string inputStr, string actionType, string trackId = null)
        {
            List<string> result = new List<string>();

            try
            {
                StringBuilder sbResult = new StringBuilder();

                HttpWebRequest Http = (HttpWebRequest)WebRequest.Create("http://fly2njoy.com/InstantPay/api/Payments/" + url);

                Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                Http.Method = "POST";
                byte[] lbPostBuffer = Encoding.UTF8.GetBytes(inputStr);
                Http.ContentLength = lbPostBuffer.Length;
                Http.Timeout = 900000; //5000 milliseconds == 5 seconds// 900000 milliseconds == 900 seconds- 15 mints
                Http.ContentType = "application/json";
                Http.Accept = "application/json";

                using (Stream PostStream = Http.GetRequestStream())
                {
                    PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        responseStream.Close();

                        result.Add("success");
                        result.Add(sbResult.ToString());

                        InstantPayDataBaseService.IPDMTInsertAllLog(inputStr, sbResult.ToString(), "http://fly2njoy.com/InstantPay/api/Payments/" + url, actionType, trackId);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add("error");
                if (!string.IsNullOrEmpty(trackId))
                {
                    result.Add(ex.Message + " TrackId : " + trackId);
                }
                else
                {
                    result.Add(ex.Message);
                }

                InstantPayDataBaseService.IPDMTIPDMTErrorLog(inputStr, ex.ToString(), "http://fly2njoy.com/InstantPay/api/Payments/" + url, actionType, trackId);
            }

            return result;
        }

        public static DataTable IPDMTFilterTransDetails(string agentId, string fromdate, string todate, string transid, string trackid)
        {
            DataTable result = new DataTable(); 

            try
            {
                result = InstantPayDataBaseService.IPDMTFilterTransDetails(agentId,fromdate, todate, transid, trackid);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
    }
}
