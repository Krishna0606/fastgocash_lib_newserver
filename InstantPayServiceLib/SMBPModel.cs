﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstantPayServiceLib
{
    public class SMBPModel
    {
        public class BBPSBillerLabelData
        {
            public string BillUpdation { get; set; }
            public string Index { get; set; }
            public string Labels { get; set; }
            public string FieldMinLen { get; set; }
            public string FieldMaxLen { get; set; }
        }


        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class LabelData
        {
            public string Index { get; set; }
            public string Labels { get; set; }
            public string FieldMinLen { get; set; }
            public string FieldMaxLen { get; set; }

        }

        public class BillerData
        {
            public string ServiceType { get; set; }
            public string Operator { get; set; }
            public string SpKey { get; set; }
            public string IsBillFetch { get; set; }
            public string BillUpdation { get; set; }
            public string BillerId { get; set; }
            public List<LabelData> LabelData { get; set; }

        }

        public class BBPSBillerList
        {
            public string ResponseCode { get; set; }
            public string ResponseMessage { get; set; }
            public List<BillerData> BillerData { get; set; }

        }
    }
}
